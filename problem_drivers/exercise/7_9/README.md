# 7_9 时制转换 12-hour to 24-hour

## 题目

编写程序要求用户输入 12 小时制的时间，然后用 24 小时制显示该时间。

## 样例

### 样例一

    Enter a 12-hour time: 9:11 PM
    Equivalent 24-hour time: 21:11

### 样例二

    Enter a 12-hour time: 4:33 AM
    Equivalent 24-hour time: 4:33

## 数据范围

参考编程题 8 中关于输入格式的描述。