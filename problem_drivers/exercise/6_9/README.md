## 第六章

9. 第二章的编程题 8 要求编程计算第一，第二，第三个月还贷后剩余的贷款金额，修改该程序，要求用户输入还贷的次数并显示每次还贷后剩余的贷款金额。

      Enter amount of loan: 20000.00
      Enter interest rate: 6.0
      Enter monthly payment: 385.66
      Enter number of payments: 3
      Balance remaining after 1 month(s): $19714.34
      Balance remaining after 2 month(s): $19427.25
      Balance remaining after 3 month(s): $19138.73