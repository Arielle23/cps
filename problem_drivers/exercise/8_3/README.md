# 8_3 修改 redigit.c Modify repdigit.c

## 题目

修改 8.1 节的程序 repdigit.c ，使得用户可以录入多个数进行重复数字的判断。当用户录入的数小于或等于0时，程序终止。

## 样例

### 样例一

    Enter a number: 9938422
    Repeated digit(s): 2 9

    Enter a number: 384533
    Repeated digit(s): 3

    Enter a number: 912
    Repeated digit(s):

    Enter a number: 0

### 样例二

    Enter a number: 89344
    Repeated digit(s): 4

    Enter a number: 111234
    Repeated digit(s): 1

    Enter a number: 98832
    Repeated digit(s): 8

    Enter a number: 0

## 数据范围

输入数据为long范围内的数据。