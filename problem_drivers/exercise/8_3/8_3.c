#include <stdio.h>
#include <string.h>

#define NUM 10

int main(void)
{
    int no_of_digits_seen[NUM];
    int digit;
    long n;
    for (;;) {
        /* no_of_digits_seen[] = {0};     */
        memset(no_of_digits_seen,0,sizeof(int) * NUM);
        printf("Enter a number: ");
        scanf("%ld", &n);
        if (n <= 0)
            break;
        while (n > 0) {
            digit = n % 10;
            no_of_digits_seen[digit]++;
            n /= 10;
        }
        printf("Repeated digit(s):");
        for (int i = 0; i < 10; i++) {
            if(no_of_digits_seen[i] > 1)
                printf(" %d",i);
        }
        printf("\n\n");
    }
    return 0;
}

