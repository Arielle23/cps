#include <stdio.h>
#include <ctype.h>

int main(void)
{
    int i, letter_count[26] = {0};
    char ch;
    int is_anagram = 1;

    printf("Enter first word: ");
    for (; (ch = getchar()) != ' ' && ch != '\n';){
        if(isalpha(ch))
            letter_count[toupper(ch) - 'A']++;
    }
   
    printf("Enter second word: ");

    for ( ;(ch = getchar()) != ' ' && ch != '\n';){
        if(isalpha(ch))
            letter_count[toupper(ch) - 'A']--;
    }

    for (i = 0; i < 26; i++) {
        if (letter_count[i] != 0) {
            is_anagram = 0;
            break;
        }  
    }

    printf("The words are ");
    if (is_anagram)
        printf("anagrams.");
    else
        printf("not anagrams.");
    
    printf("\n");
    return 0;
}
