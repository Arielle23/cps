### 7-13：

 

编写程序计算句子的平均词长：

		Enter a sentence: It was deja vu all over again.
		Average word length: 3.4
简单起见，程序中把标点符号看作其前面单词的一部分。平均词长显示一个小数位。
