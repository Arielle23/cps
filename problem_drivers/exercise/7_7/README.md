# 7_7 修改addfrac Modify addfrac

## 题目

修改第 3 章的编程题 6，使得用户可以对两个分数进行加、减、乘、除预算（在两个分数之间输入 + 、 - 、 * 或 / 符号）。

## 样例

### 样例一

    Enter the two fractions: 1/2 + 1/3
    The sum is 5/6

### 样例二

    Enter the two fractions: 2/3 * 4/5
    The sum is 8/15

### 样例三

    Enter the two fractions: 2/3 - 2/3
    The sum is 0

### 样例四

    Enter the two fractions: 1/4 - 3/4
    The sum is -8/16

## 数据范围

分子分母均为正整数，结果不需要约分。