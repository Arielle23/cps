# 8_1 修改 repdigit.c Modify repdigit.c

## 题目

修改 8.1 节的程序 repdigit.c ，使其可以显示出那些数字有重复（如果有的话）。

## 样例

### 样例一

    Enter a number: 939577
    Repeated digit(s): 7 9

### 样例二

    Enter a number: 34
    Repeated digit(s):  

## 数据范围

输入的数据都为数字组合，长度至少为0，最长为100。
