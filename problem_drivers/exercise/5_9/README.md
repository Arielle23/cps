### 	编程题5.9 编写一个程序，提示用户输入两个日期，然后显示哪一个日期更早：

	Enter first date (mm/dd/yy): 3/6/08
	Enter second date (mm/dd/yy): 5/17/07
	5/17/07 is earlier than 3/6/08
###

> 假设所有年份的2月只有28天;

> 要求当输入错误时，主函数立刻返回-1;

	Enter first date (mm/dd/yy): 3/6/100

###
	Enter first date (mm/dd/yy): 3/6/08
	Enter second date (mm/dd/yy): 2/29/08

> #### 要求：
> 学生应提交一份文件名为 "judgeYearEarlier.c" 的完整程序
