#include <stdio.h>

int validateDate(int month, int day, int year);

int validateDate(int month, int day, int year){
	int days[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	if(month < 1 || month > 12){
		return -1;
	}
	if(year < 0 || year > 99){
		return -1;
	}
	if(day < 0 || day > days[month - 1]){
		return -1;
	}
	return 0;
}

int main(){
	int month_1 = 0;
	int day_1 = 0;
	int year_1 = 0;
	int month_2 = 0;
	int day_2 = 0;
	int year_2 = 0;
	int earlier = 1;

	printf("Enter first date (mm/dd/yy): ");
	scanf("%d/%d/%d", &month_1, &day_1, &year_1);
	if(validateDate(month_1, day_1, year_1)){
		return -1;
	}
	printf("Enter second date (mm/dd/yy): ");
	scanf("%d/%d/%d", &month_2, &day_2, &year_2);
	if(validateDate(month_2, day_2, year_2)){
		return -1;
	}

	if(year_2 < year_1){
		earlier = 2;
	}
	else if(year_2 == year_1){
		if(month_2 < month_1){
			earlier = 2;
		}
		else if(month_2 == month_1){
			if(day_2 < day_1){
				earlier = 2;
			}
		}
	}

	if(earlier == 1){
		printf("%d/%d/%02d is earilier than %d/%d/%02d\n", month_1, day_1, year_1, month_2, day_2, year_2);
	}
	else{
		printf("%d/%d/%02d is earilier than %d/%d/%02d\n", month_2, day_2, year_2, month_1, day_1, year_1);
	}
	return 0;
}
