## 第六章

4. 在 5.2 节的 broker.c 程序中添加循环，以便用户可以输入多笔交易并且程序可以计算每次的佣金。程序在用户输入的交易额为 0 时终止:

        Enter value of trade: 30000
        Commission: $166.00

        Enter value of trade: 20000
        Commission: $144.00

        Enter value of trade: 0