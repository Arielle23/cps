﻿### 第 13 章编程题 3

修改 8.2 节的 deal.c程序，使它显示出牌的全名：
     
     Enter number of cards in hand: 5
     Your hand:
     Seven of clubs
     Two of spades
     Five of diamonds
     Ace of spades
     Two of hearts
**提示**：用指向字符串的指针的数组来替换数组 rank_code 和数组 suit_code。



