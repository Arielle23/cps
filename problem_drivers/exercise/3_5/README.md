### 3-5：

修改3.2节的`addfrac.c`程序，使用户可以同时输入两个分数，中间用加号隔开：

        Enter two fractions separated by a plus sign: 5/6+3/4
        The sum is 38/24