#include<stdio.h>
void pay_amount(int dollars,int *twenties,int *tens,int *fives,int *ones)
{
    *twenties = dollars / 20;
	*tens = (dollars - *twenties * 20) / 10;
	*fives = (dollars - *twenties * 20 - *tens * 10) / 5;
	*ones = (dollars - *twenties * 20 - *tens * 10 - *fives * 5);
	
	
	
}
int main()
{
	int amount, bill_20, bill_10, bill_5, bill_1;
	printf("Enter a dollar amount:");
	scanf("%d", &amount);
	pay_amount(amount,&bill_20, &bill_10, &bill_5, &bill_1);
	printf("\n$20 bills: %d\n$10 bills: %d\n $5 bills: %d\n $1 bills: %d", bill_20, bill_10, bill_5, bill_1);
	return 0;
}
