#include <stdio.h>

int main(){
	int num = 0;
	int count = 0;
	printf("Enter a number: ");
	scanf("%d", &num);
	int num_bak = num;
	while(num) {
		num /= 10;
		count++;
	}
	printf("The number %d has %d digits\n", num_bak, count);
	return 0;
}
