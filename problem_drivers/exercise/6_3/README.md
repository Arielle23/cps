## 第六章

3. 编写程序，要求用户输入一个分数，然后将其约分为最简分式：

        Enter a fraction: 6/12
        In lowest terms: 1/2

    提示：为了把分数约分为最简分式，首先计算分子和分母的最大公约数，然后分子和分母除以最大公约数。
