### 项目21_1：
1. 编写一个程序声明结构 s（见 21.4 节），并显示出成员 a、b、c 的大小和偏移量。（使用 sizeof 来得到大小，使用 offset 来得到偏移量。）同时使程序显示出整个结构的大小。根据这些信息，判断结构中是否包含空洞。如果包含空洞，指出每一个空洞的位置和大小。

		a的大小: 1,  a的偏移量：0
		b的大小: 8,  b的偏移量：4
		c的大小: 4,  c的偏移量：12
		空洞的总大小:3
	因编译器差异，所以不必再输出位置，只需要输出总的空洞大小即可