#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#define N 8
#define SUM_MINUTE 1440
#define BUFFERSIZE 40

struct flight {
	char *departure;
	char *arriving;
	int  time;
};

const struct flight search_flight[] =
	{{"8:00 a.m.", "10:16 a.m.", 480},
     {"9:43 a.m.", "11:52 a.m.", 583},
     {"11:19 a.m.", "1:31 p.m.", 679},
     {"12:47 p.m.", "3:00 p.m.", 767},
     {"2:00 p.m.", "4:08 p.m.", 840},
     {"3:45 p.m.", "5:55 p.m.", 945},
     {"7:00 p.m.", "9:20 p.m.", 1140},
     {"9:45 p.m.", "11:58 p.m.", 1305},
	};

int main(int argc, char *argv[])
{
	int number1,number2,result;
	int index = 0;
	printf("Enter a 24-hour time: ");
    scanf("%d:%d",&number1,&number2);
	result = number1 * 60 + number2;
    for(; index < N && search_flight[index].time < result; index++)
        ;
    if(index == 0)
        index = (abs(result - search_flight[0].time) > (SUM_MINUTE - search_flight[N - 1].time + result)) ? (N - 1) : 0;
    else if(index == N)
        index--;
    else
        if(abs(result - search_flight[index].time) > abs(result - search_flight[index - 1].time))
            index--;
	printf("Closest departure time is %s, arriving at %s\n", search_flight[index].departure, search_flight[index].arriving);
	return 0;
}

