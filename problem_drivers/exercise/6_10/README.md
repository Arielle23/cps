## 第六章

10. 第 5 章的编程题 9 要求编写程序判断哪个日期更早。泛化该程序，使用户可以输入任意个日期，用 0/0/0 提示输入结束，不再输入日期:

        Enter a data(mm/dd/yy): 3/6/08
        Enter a data(mm/dd/yy): 5/17/07
        Enter a data(mm/dd/yy): 6/3/07
        Enter a data(mm/dd/yy): 0/0/0
        5/17/07 is the earliest data