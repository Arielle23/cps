#include<stdio.h>
#include<stdlib.h>
#include<time.h>
int main(void){
	struct tm t1;
	struct tm t2;
	t1.tm_sec=t1.tm_min=t1.tm_hour=0;
	t1.tm_isdst= -1;
	printf("Enter first date: ");
	
	printf("Enter month (1-12): ");
	scanf("%d",&t1.tm_mon);
	t1.tm_mon--;
	printf("Enter day (1-31): ");
	scanf("%d",&t1.tm_mday);
	
	printf("Enter year: ");
	scanf("%d",&t1.tm_year);
	t1.tm_year-=1900;
	long time1=mktime(&t1);
	
	t2.tm_sec=t2.tm_min=t2.tm_hour=0;
	t2.tm_isdst=-1;
	
	printf("Enter second date: ");
	
	printf("Enter month (1-12): ");
	scanf("%d",&t2.tm_mon);
	t2.tm_mon--;
	
	printf("Enter day (1-31): ");
	scanf("%d",&t2.tm_mday);
	
	printf("Enter year: ");
	scanf("%d",&t2.tm_year);
	t2.tm_year-=1900;
	long time2=mktime(&t2);
	
	double days=abs(difftime(time1,time2));
	int d=days/8;
	
	printf("The difference between two days is %d(Days)\n",d);
	return 0;
}
