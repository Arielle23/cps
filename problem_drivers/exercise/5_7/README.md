### 	编程题5.7 编写一个程序，从用户输入的4个整数中找出最大值和最小值：
	Enter four integers: 21 43 10 35
	Largest: 43
	Smallest: 10

> #### 要求：
>> 1.尽可能少的用if语句。<b> 提示：4条if语句足够了 </b>
>
>> 2.学生应提交一份文件名为 "findMinMax.c" 的完整程序
