#include <stdio.h>

int main(){
	int a,b,c,d;

	printf("Enter four integers: ");
	
	// 方法一
	scanf("%d %d %d %d", &a, &b, &c, &d);
	int max = a;
	int min = a;

	if(b > max){
		max = b;
	}
	else if(b < min){
		min = b;
	}

	if(c > max){
		max = c;
	}
	else if(c < min){
		min = c;
	}

	if(d > max){
		max = d;
	}
	else if(d < min){
		min = d;
	}

	// 方法二
	/*
	int array[4] = {0};
	scanf("%d %d %d %d", &array[0], &array[1], &array[2], &array[3]);
	int max = array[0];
	int min = array[0];
	for(int i = 1; i < 4; i++){
		if(array[i] > max){
			max = array[i];
		}
		else if(array[i] < min){
			min = array[i];
		}
	}
	*/
	printf("Largest: %d\n", max);
	printf("Smallest: %d\n", min);
}
