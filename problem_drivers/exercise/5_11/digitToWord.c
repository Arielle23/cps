#include <stdio.h>



int main(){
	int num = 0;

	printf("Enter a two-digit number: ");
	scanf("%d", &num);
/*
	if(num < 10 || num > 99){
		return -1;
	}
*/
	if(num >= 11 && num <= 19){
		char* stenDigit;
		switch(num){
			case 11:
				stenDigit = "eleven";
				break;
			case 12:
				stenDigit = "twelve";
				break;
			case 13:
				stenDigit = "thirteen";
				break;
			case 14:
				stenDigit = "fourteen";
				break;
			case 15:
				stenDigit = "fifteen";
				break;
			case 16:
				stenDigit = "sixteen";
				break;
			case 17:
				stenDigit = "seventeen";
				break;
			case 18:
				stenDigit = "eighteen";
				break;
			case 19:
				stenDigit = "nighteen";
				break;
		}
		printf("You entered the number %s.\n", stenDigit);
	}

	else {
		int single = num % 10;
		int ten = num / 10;
		char* ssingle = NULL;
		char* sten = NULL;

		switch(ten){
			case 2:
				sten = "twenty";
				break;
			case 3:
				sten = "thirty";
				break;
			case 4:
				sten = "forty";
				break;
			case 5:
				sten = "fifty";
				break;
			case 6:
				sten = "sixty";
				break;
			case 7:
				sten = "seventy";
				break;
			case 8:
				sten = "eighty";
				break;
			case 9:
				sten = "nighty";
				break;
		}
		switch(single){
			case 1:
				ssingle = "one";
				break;
			case 2:
				ssingle = "two";
				break;
			case 3:
				ssingle = "three";
				break;
			case 4:
				ssingle = "four";
				break;
			case 5:
				ssingle = "five";
				break;
			case 6:
				ssingle = "six";
				break;
			case 7:
				ssingle = "seven";
				break;
			case 8:
				ssingle = "eight";
				break;
			case 9:
				ssingle = "night";
				break;
		}
		printf("You entered the number %s-%s.\n", sten, ssingle);
	}
}
