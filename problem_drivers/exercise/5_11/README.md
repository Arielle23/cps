### 	编程题5.11 编写一个程序，要求用户输入一个两位数，然后显示该数的英文单词:

	Enter a two-digit number: 45
	You entered the number forty-five.
###

> 提示：把数分解为两个数字。用一个switch语句显示第一位数字对应的单词("twenty"、"thirty"等)用第二个switch语句显示第二位数字对应的单词。不要忘记11-19需要特殊处理。

> 输入错误时，主函数应该立即返回-1，例如

	Enter a two-digit number: 100

> #### 要求：
> 学生应提交一份文件名为 "digitToWord.c" 的完整程序