# 7-5 拼字游戏 Word Game

## 题目

在十字拼字游戏中，玩家利用小卡片组成单词，每个卡片包含字母和面值。面值根据字母稀缺程度的不同而不同。（面值有： 1 —— AEILNORSTU，2 —— DG，3 —— BCMP，4 —— FHVWY，5 —— K，8 —— JX，10 —— QZ.)编写函数通过对单词中字母的面值求和来计算单词的值。

## 样例

### 样例一

    Enter a word: pitfall
    Scrabble value: 12

### 样例二

    Enter a word: etyFsdY
    Scrabble value: 16

## 数据范围

编写的程序应该允许单词中混合出现大小写字母。

## 提示

使用 toupper 库函数。