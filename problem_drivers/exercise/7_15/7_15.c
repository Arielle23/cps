#include<stdio.h>

int main(){
    int val,result;
    printf("Enter a positive integer: ");
    scanf("%d",&val);
    result = 1;
    for(int i = 1; i <= val; i++){
        /* printf("%d * %d\n",result,i); */
        result *= i;
    }
    printf("Factorial of %d: %d\n",val,result);
    return 0;
}
