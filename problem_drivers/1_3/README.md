# 1_3 贷款计算 Loan calculator

## 题目

编程计算第一、第二、第三个月还贷后剩余的贷款金额，输出时保留两位小数。

## 样例

### 样例一

    Enter amount of loan: 20000.00
    Enter interest rate: 6.0
    Enter monthly payment: 386.66

    Balance remaining after first payment: 19713.34
    Balance remaining after second payment: 19425.25
    Balance remaining after third payment: 19135.71

### 样例二

    Enter amount of loan: 2500.50
    Enter interest rate: 5
    Enter monthly payment: 243.5

    Balance remaining after first payment: 2267.42
    Balance remaining after second payment: 2033.37
    Balance remaining after third payment: 1798.34

## 据范围

输入数据为正数。要求输出数据保留两位小数。

## 提示

每个月的贷款余额减去还款金额后，还需要加上贷款余额与月利率的乘积即利息。把用户输入的年利率转换成百分数后再除以12就是月利率。
