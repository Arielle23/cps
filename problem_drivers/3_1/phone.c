#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define BUFSIZE 128

int main(int argc, char *argv[])
{
	printf("Enter phone number: ");
	char recvbuf[BUFSIZE];
	char sendbuf[BUFSIZE];
	int length;
	char temp;

	fgets(recvbuf, sizeof(recvbuf),stdin);
	length = strlen(recvbuf);
	recvbuf[length] = '\0';

	for (int i = 0; i < length; i++) {
		temp = recvbuf[i];
		if ((temp >= 'A') && (temp <= 'Z')) {
			if (temp == 'A' || temp == 'B' || temp == 'C')
				sendbuf[i] = '2';
			else if (temp == 'D' || temp == 'E' || temp == 'F')
				sendbuf[i] = '3';
			else if (temp == 'G' || temp == 'H' || temp == 'I')
				sendbuf[i] = '4';
			else if (temp == 'J' || temp == 'K' || temp == 'L')
				sendbuf[i] = '5';
			else if (temp == 'M' || temp == 'N' || temp == 'O')
				sendbuf[i] = '6';
			else if (temp == 'P' || temp == 'Q' || temp == 'R' || temp == 'S')
				sendbuf[i] = '7';
			else if (temp == 'T' || temp == 'U' || temp == 'V')
				sendbuf[i] = '8';
			else if (temp == 'W' || temp == 'X' || temp == 'Y' || temp == 'Z')
				sendbuf[i] = '9';
		}
		else {
			sendbuf[i] = temp;
		}
	}
	sendbuf[length] = '\0';
	printf("%s\n", sendbuf);
	return 0;
}