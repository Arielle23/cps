#include <stdio.h>
int main(int argc, char *argv[])
{
    int numer1, numer2, denom1, denom2;
    printf("Enter two fractions seperated by a plus sign: ");
    scanf("%d / %d + %d / %d", &numer1, &denom1, &numer2, &denom2);
    printf("The sum is %d/%d\n", numer1 * denom2 + numer2 * denom1, denom2 * denom1);
    return 0;
}
