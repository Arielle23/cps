# 1_5 分数相加 Addfrac

## 题目

修改3.2节的addfrac.c程序，使用户可以同时输入两个分数，中间用加号隔开。

## 样例

### 样例一

    Enter two fractions separated by a plus sign: 5/6 + 3/4
    The sum is 38/24

### 样例二

    Enter two fractions seperated by a plus sign: 4/2 + 2/5
    The sum is 24/10

## 数据范围

输入数据为分子分母都为正整数的两个分数。输出结果不要求约分。